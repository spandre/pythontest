import unittest
from comparators import same_number

class TestUtilsComparators(unittest.TestCase):

    def test_same(self):
        self.assertTrue(same_number(1, 2))

if __name__ == '__main__':
    unittest.main()