def same_number(a, b):
    try:
        a_int = int(a)
        b_int = int(b)

        return a_int == b_int
    except:
        return False